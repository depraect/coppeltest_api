<?php

namespace App\Http\Controllers;
use App\Http\Requests\PhotoRequest;
use App\Models\Photo;
use Illuminate\Support\Facades\Storage;

class PhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $select = ['lat','lng','id'];
        $photos = Photo::select($select)->get();
        return response()->json($photos,200);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhotoRequest $request)
    {

        $time = time();
        $data = $request->only(['lat','lng']);
        $file = base64_decode($request->input('base64'));
        $data['base64'] = $time.".jpg";
        $photo = Photo::create($data);
        Storage::disk('local')->put($data['base64'], $file);

        return  response()->json($photo,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $photo = Photo::findOrFail($id);
        $photo->base64 = base64_encode(Storage::disk('local')->get($photo->base64));
        return response()->json($photo);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo = Photo::findOrFail($id);
        $photo->delete();
        return response()->json(['result'=>'deleted'],200);
    }
}
